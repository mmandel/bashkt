#!/bin/bash
#Marko Mandel
#Skript v2ljastab ekraanile k6ik v6imalikud PIN koodid
#lisaks salvestatakse tekstifaili koodid mis on suuremad kui sisend pin kood


#Kontrollib kas v6tmete arv on ikka 1. Kui ei ole, kuvab help message ja v2ljub.
if [ $# -ne 2 ]; then
    echo "Kasutamine: $0 1234 kaust"
    exit 1
fi

NUM=$1
KAUST=$2

#kontrollib kas pin on numeeriline ja 4 kohaline
if ! [[ "${NUM:$((1)):1}" =~ [^0-9] || "${NUM:$((1)):1}" =~ [^0-9] || "${NUM:$((2)):1}" =~ [^0-9] || "${NUM:$((3)):1}" =~ [^0-9] || $((${#NUM})) != 4 ]]; then 
    NUM1=$1
else
     echo "Viga: pinkoodis on lubamatud symbolid v6i on pin vale pikkusega"
     exit 1
fi

#echo "sisend on ok"



#Kui tee ei hakka slashiga, siis on failitee suhteline, ja see tehakse absoluutseks 
#kui hakkas tee punktiga, siis punkt eemaldatakse
if [[ ${KAUST:0:1} != "/" ]]; then
   if [[ ${KAUST:0:2} == "./" ]]; then
       KAUST=${KAUST:1}
       KAUST="$(pwd)$KAUST"
   else
       KAUST="$(pwd)/$KAUST"
   fi
fi


#(kui meie pin kood on 2ra olnud v22rtustatakse flag muutuja 1-ga)
flag="0"
#k2iakse k2bi for tsykkliga 0000 - 9999
for (( i=0; i<=9999; i++))
	do
	    OUT=""
	    pikkus=$((${#i}))          #mitme kohaline arv on
	    
	    #kui arv ei tule 4 kohaline, siis lisatakse vajalik arv 0-e ette
	    if [[ $pikkus != 4 ]]; then
	        for (( j=0; j<=3-$pikkus ; j++))
	            do
	                OUT="0$OUT"
	            done
        fi
        OUT="$OUT$i"
        
        #katkestus olukord
        if [[ $OUT == $NUM ]]; then 
            #kausta loomine 
            #Kontrollib kas kaust on olemas, vajadusel loob.
            if [ ! -d "$KAUST" ]; then
                mkdir -p "$KAUST"  > /dev/null 2>&1
                if [ $? -ne 0 ]; then
                    echo "Viga kausta loomisel"   
                    exit 4   
                fi 
            fi


            #kontrollin kas koodid.txt fail on juba olemas, kui olemas lisa number l6ppu
            if [[ -e "$KAUST/koodid.txt" ]] ; then
                k=0
                while [[ -e "$KAUST/koodid$k.txt" ]] ; do
                    let k++
                done
                KAUST="$KAUST/koodid$k.txt"
                touch $KAUST
            else 
                KAUST="$KAUST/koodid.txt"
                touch $KAUST
            fi
            
            flag="1" #fail on sissekirjutamiseks valmis
        fi #katkestuse l6pp
        
        #kui tuleb faili kirjutada
        if [[ $flag == "1" ]]; then
            echo $OUT >> $KAUST
        fi
        
        #v2ljastab koodi
        echo "$OUT"
        
	done
	
	echo "Pin koodid asuvad failis $KAUST"






